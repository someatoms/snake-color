part of snakeapp;

class Direction {
  static const int UP = 0;
  static const int RIGHT = 1;
  static const int DOWN = 2;
  static const int LEFT = 3;
}

String getRandomColor() {
    var chars = '0123456789ABCDEF';
    var color = '#';
    var ran = new Random();
    for (var i = 0; i < 3; i++ ) {
        var nr = ran.nextInt(16);
        color += chars.substring(nr, nr + 1);
    }
    return color;
}