part of snakeapp;

void save(List data) {
  if(!isApp) {
    var jsonString = stringify(data);
    window.localStorage['highscore'] = jsonString;
  }
}

List load() {
  if(!isApp) {
    var jsonString = window.localStorage['highscore'];
    return parse(jsonString);
  }
}

loadLS(){
  if(!isApp) {
    return window.localStorage['highscore'];
  } else {
    return null;
  }
}