part of snakeapp;

class Apple { 
  static final int DEFAULT_SIDE = 50;
  static final int DEFAULT_GROWTH = 300;
  ElemBox box;
  Rect rect;
  
  Apple(){
    var pos = getRandomPos();
    rect = new Rect(pos.x, pos.y, DEFAULT_SIDE, DEFAULT_SIDE);
    box = new ElemBox(query('#canvas'), pos.x, pos.y, DEFAULT_SIDE, DEFAULT_SIDE);
    box.elem.classes.add('apple');  
   
  }
  
  void remove(){
    box.elem.remove();
  }
  
  Point getRandomPos(){
    Random ran = new Random();
    var maxX = canvasWidth.toInt() - DEFAULT_SIDE;
    var maxY = canvasHeight.toInt() - DEFAULT_SIDE;
    var x = ran.nextInt(maxX);
    var y = ran.nextInt(maxY);
    return new Point(x, y);
  }
}