library snakeapp;

import 'package:js/js.dart' as js;
import 'dart:html';
import 'dart:async';
import 'dart:math';
import 'dart:json';

part 'backend/segment.dart';
part 'common/common.dart';
part 'backend/snake.dart';
part 'backend/controller.dart';
part 'common/elem_box.dart';
part 'backend/apple.dart';
part 'backend/others.dart';
part 'common/storage.dart';

Snake snake;
Apple apple;
num get canvasHeight => query('#canvas').getBoundingClientRect().height;
num get canvasWidth => query('#canvas').getBoundingClientRect().width - 300;
num score;
List<num> highscores;
Timer timer;
bool isApp = false;

void main() {
  initHighscore();
  score = 0;
  snake = new Snake();
  spawnApple();
  
  timer = new Timer.periodic(new Duration(milliseconds: 5), 
      (Timer timer) => update());
  KeyController keyc = new KeyController();
  window.onKeyDown.listen(keyc.keyListener);
  if(isApp) {
    js.context.chrome.app.window.current().maximize();
  }
}

void initHighscore(){
  var ls = loadLS();
  if(ls == null) {
    highscores = new List();
    for(int i = 0; i < 5; i++) {
      highscores.add(0);
    }
  } else {
    highscores = load();
    while(highscores.length < 5) {
      highscores.add(0);
    }
  }
  printHighscores();
}

void spawnApple() {
  apple = new Apple();
  if(snake.intersects(apple.rect)) {
    apple.remove();
    spawnApple();
  }
}

void update(){
  var moveRect = snake.calcMoveRect();
  
  //Move snake
  if(isOutsideCanvas(moveRect) || isCannibal(moveRect)) {
    gameOver();
    //snake.segments.last.expandToWall(canvas.getBoundingClientRect());
  } else {
    snake.move();

  }
  
  //Check for apples
  if (apple.rect.intersects(moveRect)){
    apple.remove();
    spawnApple();
    score += 100;
    query('#score').text = 'Score: $score';
    snake.growLength += Apple.DEFAULT_GROWTH;
  }
  
}

void gameOver(){
  query('#canvas').children.clear();
  snake = new Snake();
  apple = new Apple();
  updateHighscore(score);
  printHighscores();
  score = 0;
  query('#score').text = 'Color Snake';
}

void updateHighscore(num score){
  if(score > 0) {
    highscores.add(score);
    highscores.sort((a, b) => b.compareTo(a));
    if(highscores.length > 5) {
      highscores.length = 5;
    }
  }
  highscores.length = 5;
  save(highscores);
}

void printHighscores(){
  query('#highscores').children.clear();
  for(int i = 0; i < 5; i++) {
    DivElement elem = new DivElement();
    score = highscores[i];
    if(highscores[i] == 0) {
      elem.text = "${i+1}. -";
    } else if(highscores[i] < 500) {
      elem.text = "${i+1}. ${highscores[i]} Bad";
    } else if(highscores[i] < 1000) {
      elem.text = "${i+1}. ${highscores[i]} Average";
    } else if(highscores[i] < 1500) {
      elem.text = "${i+1}. ${highscores[i]} Cool";
    } else if(highscores[i] < 2000) {
      elem.text = "${i+1}. ${highscores[i]} Awesome!";
    } else if(highscores[i] > 2500) {
      elem.text = "${i+1}. ${highscores[i]} Cheater!!!";
    }
    elem.classes.add('highscore');
    query('#highscores').children.add(elem);
  }
}

bool isOutsideCanvas(Rect moveRect){
  return moveRect.left <= 0 || moveRect.top <= 0 
      || moveRect.top + moveRect.height >= canvasHeight 
      || moveRect.left + moveRect.width >= canvasWidth;
}

bool isCannibal(Rect moveRect){
  for(int i = 0; i < snake.segments.length; i++){
    if(i != snake.segments.length - 2 && i != snake.segments.length - 1) {
      if(snake.segments[i].intersects(moveRect)) {
        return true;
      }
    }
  }
  return false;
}




